//
// Created by Akhmad Fauzie on 01/12/2019.
//

#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

string satuan[] = { "", "Satu ", "Dua ", "Tiga ", "Empat ", "Lima ", "Enam ", "Tujuh ", "Delapan ", "Sembilan ","Sepuluh ","Sebelas " };

string terbilang(int b)
{
    if (b < 0) {
        return "Minus " + terbilang(-b);
    }
    else if (b<=11) {
        return satuan[b];
    }

    else if ((b>11)&&(b<=19)) {
        return terbilang(b%10) +" Belas";

    }

    else if ((b>=20)&&(b<=99)) {
        return terbilang(b/10) + "Puluh " + terbilang(b%10);
    }

    else if ((b>=100)&&(b<=199)) {
        return "Seratus " + terbilang(b%100);
    }

    else if ((b>=200)&&(b<=999)) {
        return terbilang(b/100) + "Ratus " + terbilang(b%100);
    }

    else if ((b>=1000)&&(b<=1999)) {
        return "Seribu " + terbilang(b%1000);
    }

    else if ((b>=2000)&&(b<=9999)) {
        return terbilang(b/1000) + "Ribu " + terbilang(b%1000);
    }

    else if ((b > 9999)) {
        cout<<"Angka harus kurang dari 9999";
    }

    return 0;
}

string file(string nama) {
    return nama + ".txt ";
}

int main()
{
    int nilai;
    string nama_file, status;

    awal:
        cout<<"Masukkan Nama File : ";
        cin>>nama_file;

        validasi_input:
        cout<<"================================\n";
        cout<<"Program Pengeja Bilangan 4 Digit\n";
        cout<<"================================\n";
        cout<<"Masukkan Bilangan (1-4 Digit): ";
        cin>>nilai;

        if(cin.good()) {
            cout<<"Angka Tersebut Adalah : " + terbilang(nilai);

        } else {
            cin.clear();
            cin.ignore();
            cout<<"Input tidak boleh huruf!"<<endl<<endl;
            goto validasi_input;
        }

        ofstream myFile;
        myFile.open(file(nama_file).c_str(), ios::out);
        myFile<<to_string(nilai) +  " = " + terbilang(nilai);
        myFile.close();

        validasi_perulangan:
        cout<<"\nLagi? (Y/N) \t";
        cin>>status;

        if (status == "Y" || status == "y") {
            goto awal;
        } else if (status == "N" || status == "n") {
            cout<<"\n===============================";
            cout<<"\nTerima Kasih!"<<endl;
            cout<<"===============================\n";
        } else {
            cout<<"Hanya Huruf (Y/N)"<<endl;
            goto validasi_perulangan;
        }

    return 0;
}
