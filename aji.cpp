//
// Created by Akhmad Fauzie on 09/12/2019.
//
#include <iostream>
#include <fstream>
using namespace std;

string bilangan[] = {"", "Satu ", "Dua ","Tiga ","Empat "," Lima ","Enam ","Tujuh ","Delapan ","Sembilan ", "Sepuluh ", "Sebelas "};
string eja(int angka) {
    if (angka < 0){
        return "Negatif " + eja(-angka);
    } else if (angka < 12){
        return bilangan[angka];
    } else if (angka < 20){
        return eja(angka%10) + "Belas";
    } else if (angka < 100) {
        return eja(angka/10) + "Puluh " + eja(angka%10);
    } else if (angka < 200) {
        return "Seratus " + eja(angka%100);
    } else if (angka < 1000) {
        return eja(angka/100) + "Ratus " + eja(angka%100);
    } else if (angka < 2000) {
        return "Seribu " + eja(angka%1000);
    } else if (angka < 10000) {
        return eja(angka/1000) + "Ribu " + eja(angka%1000);
    } else { return "Angka Maksimal 4 digit"; }
}

int main() {
    int angka;
    string file,ulang;
    string separator = "==========================================";
    ofstream simpan;
    cout<<separator<<endl<<"Program Pengeja Angka C++"<<endl<<separator<<endl;
    input:
    cout<<"Masukkan nama file : "; cin>>file;
    cout<<"Masukkan angka (1-4 digit) : "; cin>>angka;
    if (cin.good()) {
        cout<<angka<<" = "<<eja(angka)<<endl;
        simpan.open(file+".txt", ios::out);
        simpan<<to_string(angka) + " = " + eja(angka);
        simpan.close();
    } else {
        cout<<"Angka tidak valid"<<endl;
        cin.clear(); cin.ignore(INT8_MAX, '\n');
        goto input;
    }

    check:
    cout<<"Lagi? (Y/N) : "; cin>>ulang;
    if (ulang == "Y" || ulang == "y") {
        goto input;
    } else if (ulang == "N" || ulang == "n") {
        cout<<separator<<endl<<"Terimakasih :)"<<endl<<separator<<endl;
        return 0;
    } else{
        cout<<"Pilihan tidak valid"<<endl;
        goto check;
    }
}
